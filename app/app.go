package app

import (
	"fmt"
	"go-stoloto-jira/pkg/insights_client"
	"os"
)

type App interface {
	Start() error
	DoJob() error
}

type application struct {
	apiClient insights_client.InsightsClient
}

func (a *application) Start() error {
	root := os.Getenv("JIRA_ROOT")
	user := os.Getenv("JIRA_USER")
	password := os.Getenv("JIRA_PASSWORD")

	a.apiClient = insights_client.NewClient(root, user, password)

	return nil
}

func (a *application) DoJob() error {
	res, err := a.apiClient.GetCIIObjectByCode("label=GT_SAPCRM.2.2_00")

	if err != nil {
		return err
	}

	fmt.Printf("%v", res)

	return nil
}

func New() App {
	return &application{}
}

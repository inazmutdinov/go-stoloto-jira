package main

import (
	"go-stoloto-jira/app"
	"log"
)

func main() {
	a := app.New()

	if err := a.Start(); err != nil {
		log.Fatal(err)
	}

	log.Fatal(a.DoJob())
}

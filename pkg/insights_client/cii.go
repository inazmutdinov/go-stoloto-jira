package insights_client

type CII struct {
	GROUP_ATTRIBUTE     bool
	InsightId           string
	CIICode             string
	Status              string
	Temporary           string
	ChangeInProgress    bool
	DateToProd          string
	SourceSystem        string
	ConsumerSystem      string
	IntegrationName     string
	SourceProtocol      string
	SourceFormat        string
	ConsumerProtocol    string
	ConsumerFormat      string
	WithEsb             string
	Mode                string
	IntegrationScenario string
	Schedule            string
	Boi                 string
	BoiFormat           string
	ConfluenceUrl       string
	Comment             string
	ProtocolDescription string
	MethodQueryMessId   string
}

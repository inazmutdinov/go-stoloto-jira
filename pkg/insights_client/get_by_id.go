package insights_client

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

type SearchResult struct {
	ObjectEntries []struct {
		Id         int64  `json:"id"`
		Label      string `json:"label"`
		Attributes []struct {
			Id                    int64 `json:"id"`
			ObjectTypeAttributeId int64 `json:"objectTypeAttributeId"`
			ObjectId              int64 `json:"objectId"`
			ObjectAttributeValues []struct {
				Value          string `json:"value"`
				SearchValue    string `json:"searchValue"`
				ReferencedType bool   `json:"referencedType"`
				DisplayValue   string `json:"displayValue"`
			} `json:"objectAttributeValues"`
		} `json:"attributes"`
	} `json:"objectEntries"`
}

func (c *client) GetCIIObjectByCode(id string) (*SearchResult, error) {
	params := url.Values{}
	params.Add("objectSchemaId", "9")
	params.Add("iql", id)

	url := fmt.Sprintf("%s/%s?%s", c.root, "iql/objects", params.Encode())
	fmt.Println(url)

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	req.SetBasicAuth(c.user, c.password)

	hc := &http.Client{}
	res, err := hc.Do(req)
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	log.Println(string(data))

	var val SearchResult

	if err = json.Unmarshal(data, &val); err != nil {
		return nil, err
	}

	return &val, nil

}

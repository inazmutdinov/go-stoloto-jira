package insights_client

type InsightsClient interface {
	GetCIIObjectByCode(id string) (*SearchResult, error)
}

type client struct {
	root     string
	user     string
	password string
}

func NewClient(root, user, password string) InsightsClient {
	return &client{root, user, password}
}
